#!/usr/bin/env python

# Copyright 2020 Theo Rieken
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


"""Framework

This module is representing the general biological Framework of interest. It
consists of ModuleElements that interact with each other. The Framework is
modeled as a directed graph that implicitly encodes the edges that represent
the connections between the nodes, that can be e.g. Neurons or Receptors.

Author: Theo Rieken
Date: 02.02.2020

Todo:
    * include helper for dynamic programming approach to complex mathematical equations
    * think of automated way to reduce complexity in node computation (e.g. reuse part results)

"""


import shutil, time, abc, json, os, copy, curses, importlib, inspect
import numpy as np
import matplotlib.pyplot as plt
from .vars import vars


def check_data_set(data, form, loc: str):
    """Method checks the input json data for correctness.

    Note:
        This is a recursive method that will raise KeyExceptions for every key
        that is contained in self.system_structure but not in the given json
        data describing the Framework of interest.

    Args:
        data (dict): input is given json data
        form (various): form data needs to match
        loc (str): the current path for error messages

    Raises:
        ValueError: at path the value does not match needed type
        KeyError: a certain key is missing in the data object given
    """

    if type(form) is not dict:
        # just check if data matches form
        if type(data) is not form:
            raise ValueError(terminal_colors.FAIL + "ERROR: path <" + str(loc) + "> does not match type " + str(form) + terminal_colors.ENDC)
    else:
        # if not end position reached, recursion
        for key in list(form.keys()):
            # check if key means iteration of direct call
            if key == '<many>':
                # create local variable
                counter = 0
                # iterate though all sub objects of key index
                for e in data:
                    # make recursive call
                    check_data_set(e, form[key], loc + "." + str(counter))
                    # increment local counter variable
                    counter += 1
            else:
                # check if key exists
                if key in data:
                    # if not many, directly check index
                    check_data_set(data[key], form[key], loc + "." + key)
                else:
                    if key.find('#') == -1:
                        raise KeyError("ERROR: needed key <" + str(key) + "> does not exist at path <" + loc[1:] + ">")
    # if method goes through return true for success
    return True


def convert_in_fs_name(name: str, is_path: bool = False):
    """Method transforms name in file Framework conform name

    Note:
        This method gets a path or name and converts it into a path or name
        that is able to be used in the file Framework (e.g. convert gaps and
        dots into subscores).

    Args:
        name (str): the input name that can be containing forbidden chars
        is_path (bool): if is activated function wont change '/' in subscore

    Returns:
        transformed_name (str): the name that is good for the file Framework
    """
    # initialize name
    transformed_name = name

    # list of replaced items
    rep = ['.', ' ']
    rem = ['[', ']']

    # replace all wanted stuff
    for r in rep:
        transformed_name = transformed_name.replace(r, '_')
    # remove all unwanted stuff
    for r in rem:
        transformed_name = transformed_name.replace(r, '')

    # when not path replace '/' with '_'
    if not is_path:
        transformed_name = transformed_name.replace('/', '')

    # transform to lower case
    transformed_name = transformed_name.lower()
    # return transformed name
    return transformed_name


def merge_parameters(out, upt, node_id: int, loc: str = ''):
    """Method updates the existing parameter dict with new keys.

    Note:
        This is a recursive method that will raise KeyExceptions for every key
        that is contained in self.system_structure but not in the given json
        data describing the Framework of interest.

    Args:
        data (dict): input is given json data
        form (various): form data needs to match
        loc (str): the current path for error messages

    Raises:
        ValueError: at path the value does not match needed type
        KeyError: a certain key is missing in the data object given
    """

    if type(upt) is not dict:
        # just check if data matches form
        out = upt
        # print update successful
        Framework.custom_params.append("Custom parameter <" + loc + " = " + str(round(upt, 2)) + "> added to node <" + str(node_id) + ">")
    else:
        # if not end position reached, recursion
        for key in list(out.keys()):
            if key in upt:
                # update the loc path
                loc = loc + key if loc == '' else loc + '.' + key
                # check if key means iteration of direct call
                out[key] = merge_parameters(out[key], upt[key], node_id, loc)
    # if method goes through return true for success
    return out


def get_info_timestep(comp_ts: float):
    """Method returns a working time step based on computation time step.

    Args:
        comp_ts (float): the computation time step

    Returns:
        info_ts (float): a time step to export data and show Framework info

    """

    # create variable for info ts
    if comp_ts <= 0.001:
        info_ts = 0.01
    elif comp_ts <= 0.1:
        info_ts = 0.5
    elif comp_ts <= 10:
        info_ts = 1
    elif comp_ts <= 100:
        info_ts = 10
    elif comp_ts <= 10000:
        info_ts = 100
    else:
        info_ts = 1000
    # return the info timestamp
    return info_ts


def get_node_class_module(node: str, path: str = 'Framework'):
    for entry in os.scandir(path):
        blabla = entry.path
        if entry.is_file():
            if entry.path.endswith(".py") and not entry.name.startswith(".") and not (entry.name == '__init__.py'):
                # create import path
                module_loc = str(entry.path).replace('/', '.').replace('\\', '.').replace('.py', '')
                # iterate through contained modules
                for name, cls in inspect.getmembers(importlib.import_module(module_loc), inspect.isclass):
                    # check if name matches
                    if name == node:
                        # return success
                        return module_loc
        else:
            # recursive call to search further in library
            import_success = get_node_class_module(node, entry.path)
            # check if found
            if import_success is not None:
                return import_success
    # if gets through
    return None


class terminal_colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class node_states:
    STANDBY = 0
    COMPUTING = 1
    HAS_ERROR = 2


class ChemicalSignal:

    # Growth Hormone Releasing Hormone
    GHRH = None
    # Growth Hormones
    GH = None
    # Somatastatin
    SS = None
    # Insulin-like growth factor 1
    IGF_1 = None

    # Constructor of the endocrine signal
    def __init__(self):
        # iterate through all keys and set zero
        for key in [a for a in dir(self) if not a.startswith('__')]:
            setattr(self, key, 0)


class Origin(abc.ABC):
    """Class represents macroscopic element of the biological information Framework

    Notes:
        The origin is the object that the single nodes, that operate on a more
        detailed level belong to.

    Attributes:
        name (str): the name of the Framework element
        location (str): the location of the Framework element in whole Framework
        id (int): the id of the origin element
    """

    # local attributes
    id = None
    name = None
    location = None

    # static attributes for checking
    used_ids = []

    def __init__(self, id: int, name: str, location: str):
        """Constructor of origin class.

        Note:
            This class represents all macroscopic elements of the biological
            signaling Framework. Nodes are mostly sub parts of the origin objects.
            The location string has sub origins (also represented in output
            file structure) seperated by dot e.g. Body.CNS.HypogastricNerve

        Arguments:
            id (int): the identification key of object
            name (str): the name of the origin object
            location (str): the location in the body
        """

        # check if id has not been used before
        if id in self.used_ids:
            raise ValueError(terminal_colors.FAIL + "ERROR: origin id <" + str(id) + "> has already been used" + terminal_colors.ENDC)
        else:
            self.id = id
            self.name = name
            self.location = location

    @abc.abstractmethod
    def get_nodes(self):
        """Method returns all nodes from origin

        Returns:
            nodes (list): a list of all nodes of origin
        """
        pass

    @abc.abstractmethod
    def add_node(self, node):
        pass


class Node(abc.ABC):
    """Class Node represents a biological element of the Framework.

    Nodes can be Neurons, Receptors and more and have common attributes.
    They are the central and most abstract form of describing every piece of the
    model and all interactions with the elements are done by this abstract version
    of the systems elements.

    Note:
        self.ids is a statically defined list that is used for checking if the
        given id of an element really is unique (if not contained in the list
        so far).

    Attributes:
        name (str): name of the systems sub element
        is_input_node (bool): stating whether element gets Framework input
        data_fields (list): all data fields of node
        data_store (np.array): numpy array containing all nodes data
        last_calculation_time (float): the last time a calculation happened
        last_calculation_output (int): number of iterations since last saving of data

    Raises:
        ValueException when id has already been used in the Framework

    Todo:
        * dynamic programing interface for other nodes of Framework

    """

    # general attributes
    id = None
    name = None
    is_input_node = None

    # local attributes for numerical handling
    last_calculation_time = None
    last_calculation_output = None

    # attributes for data handling
    data_fields = None
    data_store = None
    data_quantity = None

    # node inputs
    chemical_input = None
    electrical_input = None

    # list storing the adjacent nodes
    electrical_targets = None
    chemical_targets = None

    # store node parameters
    parms = None

    # finite state logic for nodes
    state = None
    has_errors = None
    error = None

    # statically defined list of ids
    used_ids = []

    def __init__(self, id: int, isi: bool = False, name: str = None, p: dict = {}):
        """Constructor of node object

        Note:
            data_fields must be initialized in the derived classes constructors

        Arguments:
            id (int): the identification key of node
            isi (bool): stating whether node is Framework input
            name (str): the name of the node
        """

        # first, check if id has already been used
        if id not in self.used_ids:
            # if id has not been used so far, append it to self.ids
            self.used_ids.append(id)

            # overwrite params if there
            self.parms = p

            # initiate local attributes of model element
            self.id = id
            self.is_input_node = isi
            self.last_calculation_time = 0
            self.last_calculation_output = 0

            # initiate inputs to node
            self.chemical_input = {}
            self.electrical_input = {}

            # initiate state
            self.state = node_states.STANDBY
            self.has_errors = False
            self.error = None
            self.data_quantity = 0

            # handle name
            if name is None:
                self.name = "Node " + str(self.id) + ' (' + self.__class__.__name__ + ')'
            else:
                self.name = name

            # initiate targets list for target elements
            self.electrical_targets = []
            self.chemical_targets = []
        else:
            # id id has been used already raise ValueException
            raise ValueError(terminal_colors.FAIL + "ERROR: id <" + str(id) + "> has already been used" + terminal_colors.ENDC)

    def add_electrical_target_node(self, node):
        """Method sets a new electrical target to the model element.

        Args:
            node (Node): the adjacent node as the target
        """

        # import node class
        from .core import Node

        # check for type of target
        assert issubclass(type(node), Node)

        # add to electrical targets
        self.electrical_targets.append(node)

    def add_chemical_target_node(self, node):
        """Method sets a new endocrine target to the model element.

        Args:
            node (Node): the adjacent node as the target
        """

        # import node class
        from .core import Node

        # check for type of target
        assert issubclass(type(node), Node)

        # add to electrical targets
        self.chemical_targets.append(node)

    def write_data_file(self, path: str):
        """Method writes current Framework status to file.
        """

        # check if there is any data to output
        if self.data_store is not None:

            # initiate list with indexes for two dimensional data sets
            two_dim = []

            # initiate timestamp variable containing the current timestamp
            timestamp = 0

            # initiate file dict containing all needed files
            files = {0: open(path + '/' + str(self.id) + '_' + self.__class__.__name__ + '_general.csv', 'w')}

            # iterate through dataset to see if there are two dimensional data sets contained
            head_line_main = 'time'
            for index, ds in enumerate(self.data_store[0]):
                if index > 0:
                    if type(ds) in [list]:
                        # remember index of type
                        two_dim.append(index)
                        # save index to files
                        files[index] = open(path + '/' + str(self.id) + '_' + self.__class__.__name__ + '_' +
                                            convert_in_fs_name(self.data_fields[index - 1]) + '.csv', 'w')
                        # append first line with field names
                        tmp = 'time'
                        for i, e in enumerate(ds):
                            # todo: need way of getting delta x for specific element
                            tmp += ', ' + str(i * self.parms['NerveFiber'][self.fiber]['delta_x']) + ' mm'
                        files[index].write(tmp + '\n')
                    else:
                        # append name of field
                        head_line_main += ', ' + self.data_fields[index - 1]
            # append first line
            files[0].write(head_line_main + '\n')

            # iterate through the dataset and start writing data in the files
            for dataset in self.data_store:
                # create line to write
                data_line = ''
                for index, field in enumerate(dataset):
                    if index == 0:
                        timestamp = field
                        data_line += str(timestamp) + ', '
                    elif index in two_dim:
                        sub_line = str(timestamp) + ', '
                        for ele in field:
                            sub_line += str(ele) + ', '
                        files[index].write(sub_line + '\n')
                    else:
                        data_line += str(field) + ', '
                files[0].write(data_line + '\n')

    def save_data(self, time: float, data: list):
        """Method saves data to the node data object of name.

        Note.
            If the node data object with that name has not been created method
            will create the object. If desc changes for the specific object an
            exception will be raised.

        Arguments:
            data (list): the list of all relevant data points
            time (float): the time where values are produced

        """

        if data is not None:
            # check whether data should be outputted
            if (time - self.last_calculation_output) >= get_info_timestep(self.get_node_dt()):
                # prepend time to data set
                data.insert(0, time)

                # count the amount of data coming in
                self.data_quantity += len(data)
                for e in data:
                    if type(e) is list:
                        self.data_quantity += len(e)

                # check if data storage has been initialized
                if self.data_store is None:
                    # create numpy array
                    self.data_store = np.array([data], dtype=object)
                else:
                    append_object = np.array([data], dtype=object)
                    self.data_store = np.append(self.data_store, append_object, axis=0)

                # set last output to zero again
                self.last_calculation_output = time

    def get_graph(self, selection: list):
        """Method returns a matplotlib graph object.

        Arguments:
            selection (list): the indexes of data_fields to be viewed

        Returns:
            figure (plt.fig): the figure produced by matplotlib
        """

        # create plot of selected data
        fig = plt.figure()
        fig.suptitle('Computation Data of ' + self.name)
        for i, select in enumerate(selection):
            # add subplot to figure
            ax = fig.add_subplot(len(selection), 1, i + 1)
            try:
                # create time vector
                time = self.data_store[:, 0]
                # check if select is int
                index = int(select)
                # check whether one or two dimensions
                if type(self.data_store[0][index]) == list:
                    # check for dx
                    dx = 0.1
                    if self.__class__.__name__ in ['Axon', 'Dendrite']:
                        dx = self.parms['NerveFiber'][self.fiber]['delta_x']
                    # create space and data variables
                    space = np.transpose(list(range(len(self.data_store[0][index]))))
                    space = [x * dx for x in space]
                    # evaluate min and max variables
                    min_val = 0
                    max_val = 0
                    for t in range(len(time)):
                        tmp_min = min(self.data_store[t][index][:])
                        tmp_max = max(self.data_store[t][index][:])
                        if tmp_min < min_val:
                            min_val = tmp_min
                        if tmp_max > max_val:
                            max_val = tmp_max
                    # start animation for subplot
                    for t in range(len(time)):
                        # clear old data in sib figure
                        ax.cla()
                        # set graphs properties
                        ax.set(ylim=(1.1*min_val, 1.1*max_val))
                        ax.set_xlabel('Space [mm] (t = ' + str(round(self.data_store[t][0], 2)) + ' ms)')
                        ax.set_ylabel(self.data_fields[index - 1])
                        ax.plot(space, self.data_store[t][index][:])
                        ax.grid(linewidth=0.4)
                        # write new data to canvas
                        fig.canvas.draw()
                        # wait shortly (time between saved elements)
                        plt.pause(0.0000000000001)
                else:
                    # add plot to current subplot
                    ax.plot(time, self.data_store[:, index])
                    ax.grid(linewidth=0.4)
                    ax.set_xlabel('Time [ms]')
                    ax.set_ylabel(self.data_fields[index - 1])
            except Exception as error:
                raise ValueError(terminal_colors.FAIL + "ERROR: the selection is not in correct form." + terminal_colors.ENDC)

        # return the figure containing all graphs
        return fig

    def set_electrical_input(self, sig: float, node_id: int):
        """Method sets a new input to the model element.

        Note:
            When the node id is zero, it is an outside Framework input.
        """
        # initiate new data
        new_data = {
            'sig': sig - vars['global']['V_rest'],
            'time': Framework.time
        }
        # check if input already exists
        if node_id in self.electrical_input:
            # check if new input is different from old one
            if self.electrical_input[node_id]['sig'] != sig - vars['global']['V_rest']:
                # save electrical input
                self.electrical_input[node_id] = new_data
        else:
            # save electrical input
            self.electrical_input[node_id] = new_data

    def get_electrical_input(self):
        # initiate response
        response = {
            'sig': 0,
            'time': 0
        }
        # iterate through all electrical inputs
        for i in self.electrical_input:
            response['sig'] += self.electrical_input[i]['sig']
            if self.electrical_input[i]['time'] > response['time']:
                response['time'] = self.electrical_input[i]['time']
        # shift back to membrane equilibrium
        response['sig'] += vars['global']['V_rest']
        # return response
        return response

    def set_chemical_input(self, sig: ChemicalSignal, node_id: int):
        """Method sets a new input to the model element.

        Note:
            The inputs are saved by their origin in order to later accumulate the data
            which happens when calling the get_chemical_input data.
        """
        # save the endocrine input
        self.chemical_input[node_id] = {
            'sig': sig,
            'time': Framework.time
        }

    def get_chemical_input(self):
        """Method returns the summed chemical input from all other nodes.
        """
        # create chemical output signal
        output = {
            'sig': ChemicalSignal(),
            'time': 0
        }
        # iterate through all inputs
        for input in self.chemical_input:
            # iterate through the fields of the chemical input signal
            for key in [a for a in dir(input['sig']) if not a.startswith('__')]:
                # set the field as the sum between current output and current input
                setattr(output['sig'], key, getattr(input, key) + getattr(output['sig'], key))
            # check time
            if output['time'] < input['time']:
                # set (later) chemical input time
                output['time'] = input['time']
        # return the sum of all chemical signals
        return output

    def evaluate_fsm(self):

        # handle state machine
        if self.state == node_states.STANDBY:
            # check for transition gating condition
            if self.is_node_active():
                # perform transition based actions
                self.last_calculation_time = Framework.time
                # reset the node
                self.reset_node()
                # set next state
                self.state = node_states.COMPUTING
            if self.has_errors:
                self.state = node_states.HAS_ERROR

        elif self.state == node_states.COMPUTING:
            # check for transition gating conditions
            if not self.is_node_active():
                # set next state or current node
                self.state = node_states.STANDBY
            if self.has_errors:
                # go to error state and stay there
                self.state = node_states.HAS_ERROR

        elif self.state == node_states.HAS_ERROR:
            # in error state (no transition possible from here)
            a = 0
        else:
            raise ValueError(terminal_colors.FAIL + "ERROR: state of node <" + str(self.name) + "> not recognized" + terminal_colors.ENDC)

    @abc.abstractmethod
    def is_node_active(self):
        """Method will evaluate whether a node is currently being computed

        """
        pass

    @abc.abstractmethod
    def get_node_dt(self):
        pass

    @abc.abstractmethod
    def get_custom_info(self):
        pass

    @abc.abstractmethod
    def compute(self, time: float):
        """Method calculates a model element of the biological Framework.

        Args:
            time (float): the current time of simulation

        """

        pass

    @abc.abstractmethod
    def reset_node(self):
        pass


class Framework:
    """Class System represents a biological sub Framework of the human body.

    The Framework can represent variations of the neuronal and endocrine Framework which
    is encoded in the input json file. Different types of neuronal connections can
    be modeled and calculated in a biologically plausible way. The calculation
    results will be written in numpy array format to json files in the ./out folder
    that are organized by the names of the contained neurons.

    Note:
        The model was developed as part of the bachelor thesis of Theo Rieken and
        is copyrighted 2020 by Technical University of Hamburg.

        The attribute self.system_structure stores the needed structure of the json
        input file. When importing and creating the digital twin of the described
        Framework the file will be checked against the here proposed structure.

    Attributes:
        nodes (list): list of the nodes contained in the model
        origins (list): list of the origins objects
        name (str): the name of the model (e.g. Urinary Bladder Brain Axis)
        version (str): the version of the model (status for reader)
        is_ready (bool): stating whether the model is okay to simulate

    Todo:
        * enable import of old data sets for visualization

    """

    # static variables
    time = 0
    custom_params = []

    # Framework attributes
    name = None
    version = None
    is_ready = None

    # systems elements
    nodes = None
    origins = None

    # save Framework input
    in_time = None
    in_value = None
    sim_target = None

    # definition of Framework structure (static)
    system_structure = {
        'name': str,                        # name of the biological model
        'version': str,                     # version number of the biological model
        'origins': {
            '<many>': {
                'id': int,                  # unique identification of the origin
                'name': str,                # given name to the origin element for identification
                'class': str,               # the class of the origin element (e.g. Cell)
                'location': str,            # location of origin element in the body (dotted)
                'data': dict,               # data passed to the origins constructor
            }
        },
        'nodes': {
            '<many>': {
                'id': int,                  # unique identification of the node
                'origin_id': int,           # the origin element of the node
                'is_input_node': bool,      # if true signals passed to Framework will be applied
                'class': str,               # defines the type of current node
                'electrical_edges': list,   # list of id's from adjacent (electrical) targets
                'chemical_edges': list,     # list of id's from adjacent (endocrine) targets
                'data': dict,               # data passed to the nodes constructor
                '#vars': dict,            # overwrite global parameters for current object
            }
        },
        'input': {
            'time': list,
            'value': list
        }
    }

    def __init__(self, path_system_json: str):
        """Constructor of Framework

        Args:
            path_system_json (str): the path of the systems json description

        """

        # read json file from given path
        with open(path_system_json, "r") as read_file:
            json_data = json.load(read_file)

        # initialize systems attributes (all empty)
        self.nodes = []
        self.origins = []
        self.name = ''
        self.version = ''

        try:
            # check json input data for correctness
            check_data_set(json_data, self.system_structure, "")
            # create the Framework based on adjacent matrix contained in data structure
            self.create_system(json_data)
            # set Framework ready to true
            self.is_ready = True
        except Exception as warnings:
            # print warning
            print(terminal_colors.FAIL + "ERROR: an error occured while creating the Framework" + terminal_colors.ENDC)
            print(warnings)
            # set Framework status to false
            self.is_ready = False

    def run(self):
        """Method runs the program based on fsm logic.
        """

        # initial state
        state = 'simulate'
        origin_id = -1
        node_id = -1

        # handle state machine
        while True:
            if state == 'main_menu':
                # generate promt message for main menu
                promt = "\nMain Menu - please choose action\n"
                for origin in self.origins:
                    promt += "\n(" + str(origin.id) + ") \t" + origin.name
                promt += "\n\n(" + str(len(self.origins) + 1) + ") \tExport data"
                tmp = 'Start' if Framework.time == 0 else 'Continue'
                promt += "\n(" + str(len(self.origins) + 2) + ") \t" + tmp + " computation"
                promt += "\n(" + str(len(self.origins) + 3) + ") \tExit the program"
                # get user input for origin
                print(promt)
                # get input from user
                try:
                    origin_id = input("\nPlease enter number: ")
                    origin_id = int(origin_id)
                    # check if user wants to exit
                    if origin_id == (len(self.origins) + 1):
                        path = input("\nWhat path do you want to create the output folder? ")
                        self.write_data(path)
                    elif origin_id == (len(self.origins) + 2):
                        state = 'simulate'
                    elif origin_id == (len(self.origins) + 3):
                        print("\nGoodbye!")
                        return True
                    else:
                        state = 'node_setup'
                except ValueError as error:
                    print(terminal_colors.FAIL + "ERROR: Your input was not a number. Please try again." + terminal_colors.ENDC)
            if state == 'node_setup':
                # get origin from id
                origin = self.get_origin(origin_id)
                # concatenate elements
                nodes = origin.get_nodes()
                # check if there is any data to plot
                if len(nodes) > 0:
                    # create promt message
                    mess = 'Please select the node of interest\n'
                    for node in nodes:
                        if node is not None:
                            mess += '\n(' + str(node.id) + ') \t' + node.name
                    print(mess)
                    # get input from user
                    try:
                        node_id = input("\nPlease enter number: ")
                        node_id = int(node_id)
                        state = 'values_setup'
                    except ValueError:
                        print(terminal_colors.FAIL + "ERROR: Your input was not a number. Please try again." + terminal_colors.ENDC)
                else:
                    print(terminal_colors.WARNING + "Element <" + str(origin.name) + "> does not contain any data. Try again." + terminal_colors.ENDC)
                    state = 'main_menu'
            if state == 'values_setup':
                # get node of interest
                node = self.get_node(node_id)
                # check if fields exist
                if (node.data_store is not None) and (len(node.data_store) > 0):
                    # create message
                    fields = []
                    counter = 1
                    promt = 'Which values do you want to plot? \n(Divide numbers with commas)\n'
                    for field in node.data_fields:
                        promt += '\n(' + str(counter) + ') \t' + field
                        fields.append(field)
                        counter += 1
                    try:
                        promt += '\n\n(' + str(counter + 1) + ') \tBack to main menu'
                        print(promt)
                        values_of_interest = input("\nPlease enter numbers: ")
                        if values_of_interest == str(counter + 1):
                            state = 'main_menu'
                        else:
                            values_of_interest = values_of_interest.split(',')
                            # create graph for selection
                            graph = node.get_graph(values_of_interest)
                            graph.show()
                    except ValueError as error:
                        print(terminal_colors.FAIL + "ERROR: Your input was not in correct shape. Please try again." + terminal_colors.ENDC)
                else:
                    print(terminal_colors.FAIL + "ERROR: This node does not contain any data (yet). Please try another node." + terminal_colors.ENDC)
                    state = 'main_menu'
            if state == 'simulate':
                # check if Framework is okay otherwise print error
                if self.is_ready:

                    # initiate local variables for simulation
                    time_info = Framework.time

                    # print status message
                    tmp = 'started' if Framework.time == 0 else 'continued'
                    print("\nThe computation of the Framework " + str(tmp) + " \n(press ctl+c to enter menu)")

                    # initiate curses
                    stdscr = curses.initscr()
                    curses.noecho()
                    curses.cbreak()

                    # initiate status variable
                    status = "Initiated"
                    done_message = ""

                    # loop for running the Framework
                    try:
                        curses.start_color()
                        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
                        curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
                        curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)

                        # start simulation routine
                        while True:
                            # create time output information
                            if Framework.time > time_info:
                                # clear screen
                                stdscr.clear()

                                if Framework.time < 1000:
                                    status = 'Computed %.2f ms      ' % (Framework.time)
                                elif Framework.time < 60000:
                                    status = 'Computed %.2f sec     ' % (Framework.time / 1000)
                                elif Framework.time < 3600000:
                                    status = 'Computed %.2f min     ' % (Framework.time / 60000)
                                elif Framework.time < 86400000:
                                    status = 'Computed %.2f hours   ' % (Framework.time / 3600000)
                                else:
                                    status = 'Computed %.2f days    ' % (Framework.time / 86400000)

                                # get height and width of window
                                height, width = stdscr.getmaxyx()

                                # initiate error array for getting all
                                sim_errors = []
                                for node in self.nodes:
                                    if node.state == node_states.HAS_ERROR:
                                        sim_errors.append("Computation error at <node " + str(node.id) + ">: " + str(node.error))

                                # print status
                                write_index = 0
                                stdscr.addstr(write_index, 0, "-------------------------------------------------- "
                                                "Node Activities ---------------------------------------------------")
                                write_index += 1

                                # check if terminal window is big enough
                                if width > 118:
                                    if height > 10 + len(self.nodes) + len(Framework.custom_params) + len(sim_errors):
                                        # create header information viewer
                                        stdscr.addstr(write_index, 0, "")
                                        write_index += 1
                                        stdscr.addstr(write_index, 0, "Node element")
                                        stdscr.addstr(write_index, 30, "Origin element")
                                        stdscr.addstr(write_index, 65, "Input voltage")
                                        stdscr.addstr(write_index, 83, "Data saved")
                                        stdscr.addstr(write_index, 95, "Custom")
                                        stdscr.addstr(write_index, 107, "Last active")
                                        write_index += 1
                                        stdscr.addstr(write_index, 0, "")
                                        write_index += 1

                                        # iterate through the origins
                                        for origin in self.origins:
                                            # iterate through all nodes of origin (sorted after origin)
                                            for node in origin.get_nodes():
                                                # create origin name (truncated) to view in table
                                                ori_name = (origin.name[:25] + '..') if len(
                                                    origin.name) > 25 else origin.name
                                                if node is not None:
                                                    nod_name = (node.name[:25] + '..') if len(
                                                        node.name) > 25 else node.name
                                                    if node.state == node_states.COMPUTING:
                                                        stdscr.addstr(write_index, 0, nod_name, curses.color_pair(2))
                                                    elif node.state == node_states.HAS_ERROR:
                                                        stdscr.addstr(write_index, 0, nod_name, curses.color_pair(1))
                                                    elif node.state == node_states.STANDBY:
                                                        stdscr.addstr(write_index, 0, nod_name)
                                                    stdscr.addstr(write_index, 30, ori_name)
                                                    # create information about data amount in node
                                                    if node.data_store is None:
                                                        data_count = "0"
                                                    else:
                                                        data_count = node.data_quantity
                                                        if data_count < 1000:
                                                            data_count = str(data_count)
                                                        elif data_count < 1000000:
                                                            data_count = ("%.2f" % (data_count / 1000)) + "k"
                                                        elif data_count < 1000000000:
                                                            data_count = ("%.2f" % (data_count / 1000000)) + "m"
                                                        else:
                                                            data_count = ("%.2f" % (data_count / 1000000000)) + "b"
                                                            data_count = str(data_count)
                                                    # create input potential information
                                                    if node.is_input_node:
                                                        data_input = "%.2f      " % node.get_electrical_input()['sig']
                                                    else:
                                                        data_input = "%.2f mV   " % node.get_electrical_input()['sig']
                                                    if node.last_calculation_time < 1000:
                                                        last_comp = ("%.2f" % node.last_calculation_time) + ' ms  '
                                                    elif node.last_calculation_time < 60000:
                                                        last_comp = ("%.2f" % (node.last_calculation_time / 1000)) + \
                                                                    ' s   '
                                                    elif node.last_calculation_time < 3600000:
                                                        last_comp = ("%.2f" % (node.last_calculation_time / 60000)) + \
                                                                    ' m  '
                                                    else:
                                                        last_comp = ("%.2f" % (node.last_calculation_time / 3600000)) + \
                                                                    ' h   '
                                                    stdscr.addstr(write_index, 65, data_input)
                                                    stdscr.addstr(write_index, 83, data_count)
                                                    stdscr.addstr(write_index, 95, node.get_custom_info())
                                                    stdscr.addstr(write_index, 107, last_comp)
                                                    write_index += 1

                                    else:
                                        # throw size error
                                        stdscr.addstr(write_index, 0, "")
                                        write_index += 1
                                        stdscr.addstr(write_index, 0,
                                                      "Please enlarge the window in height to view more",
                                                      curses.color_pair(1))
                                        write_index += 1

                                else:
                                    # throw size error
                                    stdscr.addstr(write_index, 0, "")
                                    write_index += 1
                                    stdscr.addstr(write_index, 0,
                                                  "Please enlarge the window in width to view more",
                                                  curses.color_pair(1))
                                    write_index += 1

                                stdscr.addstr(write_index, 0, "")
                                write_index += 1
                                stdscr.addstr(write_index, 0,"------------------------------------------------- "
                                            "Simulation Status --------------------------------------------------")
                                write_index += 1
                                # check if there are custom parameters
                                if len(Framework.custom_params) > 0 or len(sim_errors) > 0:
                                    stdscr.addstr(write_index, 0, "")
                                    write_index += 1
                                # add all custom parameters
                                for cp in Framework.custom_params:
                                    stdscr.addstr(write_index, 0, cp)
                                    write_index += 1
                                # create error messages
                                for error in sim_errors:
                                    stdscr.addstr(write_index, 0, error)
                                    write_index += 1
                                stdscr.addstr(write_index, 0, "")
                                write_index += 1
                                stdscr.addstr(write_index, 0, status)
                                stdscr.addstr(write_index, 65, "Press ctl+c to enter the main menu")
                                write_index += 1
                                # increase time info stand to match precision
                                time_info = time_info + get_info_timestep(self.get_system_timestep())

                                # refresh screen
                                stdscr.refresh()

                            # evaluate current Framework input and set it
                            self.set_system_input(self.get_current_input())

                            # initiate variable for counting active nodes
                            standby_node_count = 0

                            # compute all nodes from Framework
                            for node in self.nodes:
                                # handle nodes fsm handler
                                node.evaluate_fsm()
                                # check if node is active
                                if node.state == node_states.COMPUTING:
                                    # check if node needs computation
                                    if node.last_calculation_time <= Framework.time - node.get_node_dt():
                                        # compute node
                                        node.compute(Framework.time)
                                        # save last computation time
                                        node.last_calculation_time = Framework.time
                                else:
                                    # increment standby (or error) nodes
                                    standby_node_count += 1
                                    # set standby output
                                    for target in node.electrical_targets:
                                        target.set_electrical_input(vars['global']['V_rest'], node.id)
                                    for target in node.chemical_targets:
                                        target.set_chemical_input(ChemicalSignal(), node.id)

                            # check if simulation is completed
                            if (standby_node_count == len(self.nodes)) and (Framework.time >= max(self.in_time)):
                                done_message = "(Finished)"
                                break

                            # increment time in seconds
                            Framework.time += self.get_system_timestep()

                    except KeyboardInterrupt:
                        # just pass out of simulation operations
                        done_message = "(Paused)"
                        pass

                    finally:

                        # end viewer
                        curses.nocbreak()
                        curses.echo()
                        curses.endwin()

                        # print finish message when done
                        print("\n\n\n\n\n\n\n--------------------------------\n" + status + done_message)

                    # transition to main menu state
                    state = 'main_menu'

                else:
                    # print error message that Framework not okay
                    print(terminal_colors.FAIL + "ERROR: could not simulate Framework. There have been errors." + terminal_colors.ENDC)
                    # state transit
                    state = 'main_menu'

    def set_system_input(self, input_signal: float):
        """Method returns a dendrite for connecting to another neuronal element.

        Some really cool description for the method get_dendrite().

        Note:
            Some note for method.

        Args:
            input_signal (np.array): input array of data containing time and value

        """

        # iterate through all Framework elements and set input to every
        for node in self.nodes:
            # check if element is Framework input
            if node.is_input_node:
                # apply the input signal to element
                node.set_electrical_input(input_signal, 0)

    def get_system_timestep(self):
        # initiate result
        result = 1000
        # count computing nodes
        com_nodes = 0
        for node in self.nodes:
            if node.state == node_states.COMPUTING:
                com_nodes += 1
                tmp = node.get_node_dt()
                if tmp < result:
                    result = tmp
        # check if node timestamp has been applied
        if com_nodes == 0:
            # check if current sim time is before initial value
            if Framework.time <= min(self.in_time):
                # calculate the time needed to get to next input
                result = (min(self.in_time) - Framework.time)
                # check if same moment
                if result == 0:
                    result = 0.001
            elif Framework.time <= max(self.in_time):
                # overwrite time stamp with small timestep to go through input
                result = 0.01
        # return result
        return result

    def get_node(self, query_id: int):
        """Method gets a specific element from Framework model with id query_id.

        Arguments:
            query_id (int): the id of the element of interest

        Returns:
            element (Node): the model element of interest

        Raises:
            query_error (Exception): element with query_id cant be found

        """

        # initiate result object as none
        result = None

        # iterate through all elements and return element of interest
        for e in self.nodes:
            # check if id matches query
            if e.id == query_id:
                # save the result of the query
                result = e
                break

        # check if result has been identified
        if result is not None:
            # return result
            return result
        else:
            raise ValueError(terminal_colors.FAIL + "ERROR: node with id <" + str(query_id) + "> could not be found." + terminal_colors.ENDC)

    def get_origin(self, query_id: int):
        """Method returns the originn with id query_id

        Arguments:
            query_id (int): the id of the origin of interest

        Returns:
            result (Origin): the origin of interest

        Raises:
            error (ValueException): when the origin does not exist
        """

        # initiate local result variable
        result = None

        # iterate through origins and look for the searched origin
        for origin in self.origins:
            # check if id matches query string
            if origin.id == query_id:
                # save the result
                result = origin
                # break out of for loop
                break

        # check if result has been found
        if type(result) is None:
            # raise exception
            raise ValueError(terminal_colors.FAIL + "ERROR: the origin with id <" + str(query_id) + "> does not exist" + terminal_colors.ENDC)

        # return the result
        return result

    def get_current_input(self):
        """Method evaluates the current input to Framework
        """
        # initiate the index of the target value with zero
        current_input = 0
        # get current value index
        for i, t in enumerate(self.in_time):
            if t <= Framework.time:
                current_input = self.in_value[i]
            else:
                break
        # set signal
        return current_input

    def create_system(self, data: dict):
        """Method creates the Framework described by the json data.

        Args:
            data (dict): input json data encoded as python dict

        Raises:
            ValueError: if any value in json input does not match
            KeyError: if values are missing from needed structure

        """

        # print start line
        print("\n\nInitializing the runner and building the Framework ...")

        # check if input signal is chronological
        time_correct = sorted(data['input']['time']) == data['input']['time']
        length_correct = len(data['input']['time']) == len(data['input']['value'])

        # check if data set is okay
        if time_correct and length_correct:

            try:

                # set Framework input
                self.in_time = data['input']['time']
                self.in_value = data['input']['value']

                # save general attributes about Framework
                self.name = data['name']
                self.version = data['version']

                # create all origins
                for origin in data['origins']:
                    # get module
                    mod_loc = get_node_class_module(origin['class'])
                    # create new origin depending on class
                    if mod_loc is not None:
                        # create origin
                        mod = importlib.import_module(mod_loc)
                        o = getattr(mod, origin['class'])
                        new_origin = o(origin['id'], origin['name'], origin['location'], origin['data'])
                        # append new origin to self origins
                        self.origins.append(new_origin)
                    else:
                        raise Exception(terminal_colors.FAIL + "The origin class can not be recognized ..." + terminal_colors.ENDC)

                # create all nodes
                for node in data['nodes']:
                    # create parameters
                    p = {**vars['global'], **vars[node['class']]}
                    # check if there are custom parameters
                    if 'vars' in node:
                        # deep copy to remove references
                        c = copy.deepcopy(p)
                        # insert the new values into the parameters array
                        p = merge_parameters(c, node['vars'], node['id'])
                    # get node module from library
                    mod_loc = get_node_class_module(node['class'])
                    # check if node exists
                    if mod_loc is not None:
                        # create node from the json description
                        mod = importlib.import_module(mod_loc)
                        n = getattr(mod, node['class'])
                        new_node = n(node['id'], node['is_input_node'], node['data'], p)
                        # get origin that the node corresponds to and append it to it
                        orig = self.get_origin(node['origin_id'])
                        orig.add_node(new_node)
                        # save node in framework object
                        self.nodes.append(new_node)
                    else:
                        raise Exception(terminal_colors.FAIL + "The node class can not be recognized ..." + terminal_colors.ENDC)

                # create all edges between nodes
                for node in data['nodes']:
                    # append edge to target
                    current_node = self.get_node(node['id'])
                    # iterate through edges
                    for edge in node['electrical_edges']:
                        # get target node
                        target = self.get_node(edge)
                        # add target to node
                        current_node.add_electrical_target_node(target)
                    for edge in node['chemical_edges']:
                        # get target node
                        target = self.get_node(edge)
                        # add target to node
                        current_node.add_chemical_target_node(target)
            except Exception:
                raise
        else:
            raise Exception(terminal_colors.FAIL + "Error: false input signal (length of vectors or time chronology)" + terminal_colors.ENDC)

    def write_data(self, output_path: str, replace: bool = False):
        """Method writes all Framework data to files at path output_path.

        Args:
            output_path (str): the path where data shall be outputted to
        """

        # check if given path exists
        if os.path.exists(output_path):
            # status information output
            print("\nWriting data to filesystem ...")
            # create timestamp as string
            ts = time.strftime('%x').replace('/', '') + time.strftime('%X').replace(':', '')
            # create the output path based on Framework name and datetime
            output_path = output_path + '/' + ts + '_' + convert_in_fs_name(self.name)
            # check if folder exists and overwrite in that case
            if os.path.exists(output_path) and replace:
                shutil.rmtree(output_path)
            # create main data folder
            os.mkdir(output_path)
            # iterate through origins
            for origin in self.origins:
                # create location directory
                loc_dir = output_path + '/' + convert_in_fs_name(origin.location)
                origin_dir = loc_dir + '/' + convert_in_fs_name(origin.name)
                # create dir if not exist
                if not os.path.exists(loc_dir):
                    os.mkdir(loc_dir)
                if not os.path.exists(origin_dir):
                    os.mkdir(origin_dir)
                # iterate through the nodes of the current origin
                for node in origin.get_nodes():
                    node.write_data_file(origin_dir)
            # location will be dotted path to actual node data files
            print(terminal_colors.OKGREEN + "Files were created successfully" + terminal_colors.ENDC)
        else:
            print(terminal_colors.FAIL + "Error: The given path does not exist ..." + terminal_colors.ENDC)
