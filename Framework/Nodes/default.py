#!/usr/bin/env python

# Copyright 2020 Theo Rieken
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""Individual node

This module is intended to be used as a reference or layout on how to easily include own nodes into the framework.

Author: Theo Rieken
Date: 22.03.2020

"""

from ..core import Node, check_data_set


class YourNode(Node):
    """This can be your individual node for the framework.

    Note:
        Try auto completion in order to see the broad range of functionality
        provided by the framework. Parameters can be accessed via self.parms
        Refer to the README.md file when questions come to mind.
    """

    #  define local attributes for your node
    type = None

    # define the desired input data structure
    data_structure = {
        'needed_param_a': str,
        'needed_param_b': float
    }

    def __init__(self, id: int, isi: bool, data: dict = {}, p: dict = {}):

        # mandatory:  call to super class constructor
        super().__init__(id, isi, p)

        # mandatory: define the data set describing the node
        self.data_fields = ['some_internal_variable', 'some_ion_current']

        # try except construction for exception handling
        try:
            # automatic checking of incoming dataset
            check_data_set(data, self.data_structure, 'BiologicalElement')

            # todo: implement own functionality for node ...

        except (ValueError, KeyError):
            raise

    def compute(self, time: float):
        # todo: implement the computation of nodes physiological behavior

        # get electrical input with self.get_electrical_input()['sig']
        # get chemical input with self.get_chemical_input()['<compound>']

        # set computation output with self.save_data([...])

        system_function = 100

    def get_node_dt(self) -> float:
        # todo: return the timestamp of your component
        return 0.1

    def is_node_active(self):
        # todo: implement algorithm that returns activity status of node
        return True

    def reset_node(self):
        # todo: reset all created states back to equilibrium
        system_reset = 100

    def get_custom_info(self):
        # todo: you can use this function to display custom information
        return '-'
