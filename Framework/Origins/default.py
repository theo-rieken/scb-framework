#!/usr/bin/env python

# Copyright 2020 Theo Rieken
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""Individual origin

This module is intended to be used as a reference or layout on how to easily include own origins into the framework.

Author: Theo Rieken
Date: 22.03.2020

"""

from ..core import Origin, check_data_set


class YourOrigin(Origin):
    """This can be your individual origin for the framework.

    Note:
        Try auto completion in order to see the broad range of functionality
        provided by the framework. Refer to the README.md file when questions
        come to mind.
    """

    #  define local attributes for your origin
    type = None
    nodes = None

    # define the desired input data structure
    data_structure = {
        'needed_param_a': str,
        'needed_param_b': float
    }

    def __init__(self, id: int, name: str, loc: str, data: {}):

        # mandatory:  call to super class constructor
        super().__init__(id, name, loc)

        # try except construction for exception handling
        try:
            # automatic checking of incoming dataset
            check_data_set(data, self.data_structure, 'YourOrigin')

            # todo: implement own functionality for origin ...
            self.nodes = []

        except (ValueError, KeyError):
            raise

    def get_nodes(self) -> list:
        # todo: implement the gathering of contained nodes
        return []

    def add_node(self, node):
        # todo: custom include of nodes of origin
        self.nodes.append(node)
